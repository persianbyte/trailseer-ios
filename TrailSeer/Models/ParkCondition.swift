//
//  ParkConditions.swift
//  TrailSeer
//
//  Created by SoapMac on 1/27/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import Foundation

struct ParkCondition: Codable, Identifiable {
    var id: Int = 0
    var createdDate: String = ""
    var modifiedDate: String = ""
    var totalRiders: Int = 0
    var overallTotalRiders: Int = 0
    var averageMovingTime: Int = 282
    var averageElapsedTime: Int = 286
    var conditionsType: Int = 0
    var rainVolumeInThree: Float = 0.0
    var rainVolumneInHour: Float = 0.0
    var totalRainVolume: Float = 0.0
    var humidity: Float = 0.0
    var cloudCoverage: Int = 0
    var weatherConditions: String = "None"
    var weatherConditionsDesc: String = ""
    var weatherConditionsId: Int = 0
    var weatherTemp: Float = 274.88
}
