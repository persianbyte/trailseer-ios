//
//  ParkDM.swift
//  TrailSeer
//
//  Created by SoapMac on 1/27/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import Foundation


struct Park: Codable, Identifiable {
    var id: Int = 0
    var name: String = ""
    var modifiedDate: String = ""
    var parkConditions: [ParkCondition] = []

}
