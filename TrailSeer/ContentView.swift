//
//  ContentView.swift
//  TrailSeer
//
//  Created by SoapMac on 12/19/19.
//  Copyright © 2019 SoapCom. All rights reserved.
//

import SwiftUI


struct ContentView: View {
    @ObservedObject var parkListView: ParkListViewModel = ParkListViewModel()
    @State private var prefActiviated: Bool = false
    
    init() { print(UIDevice.current.identifierForVendor!.uuidString)
    }
    
    var body: some View {
        NavigationView {
            ParkListView()
//            VStack {
//                HiddenNavigationLink(destination: PreferencesView(), isActive: self.$prefActiviated)
//                List(self.parkListView.parks) { listedPark in
//                    CardView(title: listedPark.name, description: listedPark.name, overallTotalRiders: listedPark.parkConditions[0].overallTotalRiders)
//                    NavigationLink(destination: CardDetailsView(name: listedPark.name)) {
//                        EmptyView()
//                    }
//                    .frame(width: 0)
//                    .opacity(0)
//                }
//                .onAppear() {
//                    UITableView.appearance().separatorStyle = .none
//                    print("list appear")
//                }
//                .onDisappear() {
////                    UITableView.appearance().separatorStyle = .singleLine
//                }
//            }
//            .navigationBarTitle("TrailSeer")
//            .navigationBarItems(trailing:
//                HStack {
//                    ActivateButton(activates: self.$prefActiviated) { Image(systemName: "gear")
//                        .font(.largeTitle) }
//            })
        }
        .navigationViewStyle(StackNavigationViewStyle())
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct DetailView: View {
    var name: String
    
    var body: some View {
        Text("current name is: \(name) ")
            // 7.
            .navigationBarTitle(Text(name), displayMode: .inline)
    }
    
}
