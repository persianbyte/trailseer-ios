//
//  MathHelper.swift
//  TrailSeer
//
//  Created by SoapMac on 2/5/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import Foundation

class MathHelper {
    
    static func convertKelvinToFahrenheit(kelvin: Float) -> Float {
        let fahrenheightTemp = (kelvin) * (9/5) - 459.67
        return fahrenheightTemp;
    }
    
    static func secondsToHoursMinutesSeconds (seconds : Int) -> String {
        let hours = MathHelper.getStringFrom(seconds: seconds / 3600)
        let minutes = MathHelper.getStringFrom(seconds: (seconds % 3600) / 60)
        let seconds = MathHelper.getStringFrom(seconds: (seconds % 3600) % 60)
        let timestamp = "\(hours):\(minutes):\(seconds)"
        return timestamp
    }
    
    private func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {

            completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)

    }

    static func getStringFrom(seconds: Int) -> String {

        return seconds < 10 ? "0\(seconds)" : "\(seconds)"
    }
}
