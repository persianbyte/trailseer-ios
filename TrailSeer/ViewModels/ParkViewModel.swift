//
//  ParkDetailsViewModel.swift
//  TrailSeer
//
//  Created by SoapMac on 1/29/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import Foundation
import SwiftUI
import Combine


class ParkViewModel: ObservableObject {
    @Published var id: Int
    @Published var name: String
    @Published var totalRiders: Int
    @Published var overallTotalRiders: Int
    
    init(park: Park) {
        self.id = park.id
        self.name = park.name
        self.totalRiders = park.parkConditions[0].totalRiders
        self.overallTotalRiders = park.parkConditions[0].overallTotalRiders
    }
    
    func getParkDetails() {
        
    }
}
