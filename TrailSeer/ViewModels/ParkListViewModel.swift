//
//  ParkListViewModel.swift
//  TrailSeer
//
//  Created by SoapMac on 1/27/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

class ParkListViewModel: ObservableObject {
    @Published var parks: [Park] = []
    
    init() {
        //self.getParks()
    }
    
    public func getParks() {
        ParkService().getParks(){ parks in
            print(parks)
            self.parks = parks
        }
    }
    
}
