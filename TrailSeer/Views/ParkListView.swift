//
//  ParkListView.swift
//  TrailSeer
//
//  Created by SoapMac on 1/29/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import SwiftUI

struct ParkEx: Identifiable{
    var id  = UUID()
    var name = String()
    var description = String()
    var conditionStatus = String()
}

struct ParkListView: View {
    @ObservedObject var parkListView: ParkListViewModel = ParkListViewModel()
    @State private var prefActiviated: Bool = false
    
    let parks: [ParkEx] = [
        ParkEx(name: "Walnut Creek", description: "Desc"),
        ParkEx(name: "Brushy Creek", description: "Desc")]
    
    var body: some View {
        VStack {
            HiddenNavigationLink(destination: PreferencesView(), isActive: self.$prefActiviated)
            List(self.parkListView.parks) { listedPark in
                CardView(park: listedPark)
                NavigationLink(destination: CardDetailsView(park: listedPark)) {
                    EmptyView()
                }
                .frame(width: 0)
                .opacity(0)
            }
            .onAppear() {
                UITableView.appearance().separatorStyle = .none
                print("list appear")
                self.parkListView.getParks()
            }
            .onDisappear() {
                //                    UITableView.appearance().separatorStyle = .singleLine
            }
        }
        .navigationBarTitle("TrailSeer")
        .navigationBarItems(trailing:
            HStack {
                ActivateButton(activates: self.$prefActiviated) { Image(systemName: "gear")
                    .font(.largeTitle) }
        })
    }
}

struct ParkListView_Previews: PreviewProvider {
    static var previews: some View {
        ParkListView()
    }
}
