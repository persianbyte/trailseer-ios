//
//  CardView.swift
//  TrailSeer
//
//  Created by SoapMac on 1/5/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import SwiftUI
import MapKit

struct CardView: View {
    @State private var show_modal: Bool = false
    
    var park: Park
    
    var body: some View {
        VStack {
            Image("walnutcreek2")
                .resizable()
                .aspectRatio(contentMode: .fit)
            HStack {
                VStack(alignment: .leading) {
                    HStack {
                        Text(self.park.name)
                            .font(.headline) .foregroundColor(.secondary)
                        Spacer()
                        HStack {
                            Circle()    .fill(Color.green)   .frame(width: 10, height: 10)
                            Text("Dry")
                            
                        }
                        
                    }
                    Text("Total riders today:  \(self.park.parkConditions[0].overallTotalRiders)")
                        .foregroundColor(.primary)
                        .lineLimit(2)
                    HStack {
                        Button(action: {
                        }) {
                            Text("Send Feedback")
                                .font(.subheadline)    .foregroundColor(.accentColor)
                        }.onTapGesture {
                            print("Button Pushed")
                            self.show_modal = true
                        }
                        .sheet(isPresented: self.$show_modal) {
                            ParkFeedbackModal().onAppear() {
                                print("appear feedback")
                            }
                        }
                        Spacer()
                        Button(action: {
                        }) {
                            Image(systemName: "map")
                                .font(.body) .foregroundColor(.accentColor)
                        }.onTapGesture {
                            openMapForPlace()
                        }
                        
                    }
                    Text("Last Updated On: \(park.parkConditions[0].modifiedDate.UTCToLocal(incomingFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outGoingFormat: "MMM d, yyyy h:mm a"))".uppercased())
                        .font(.caption)
                        .foregroundColor(.secondary)
                    
                }
                .layoutPriority(100)
                Spacer()
            }
            .padding()
        }
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1)
        )
            .padding([.top, .horizontal])
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(park: Park(name: "Park", parkConditions: [
            ParkCondition()
        ]))
    }
}

func openMapForPlace() {
    
    let lat1 : NSString = "30.4007"
    let lng1 : NSString = "-97.6831"
    
    let latitude:CLLocationDegrees =  lat1.doubleValue
    let longitude:CLLocationDegrees =  lng1.doubleValue
    
    let regionDistance:CLLocationDistance = 1000
    let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
    let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
    let options = [
        MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
        MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
    ]
    let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
    let mapItem = MKMapItem(placemark: placemark)
    mapItem.name = "\("Walnut Creek Austin")"
    mapItem.openInMaps(launchOptions: options)
    
}
