//
//  CardDetailsView.swift
//  TrailSeer
//
//  Created by SoapMac on 1/9/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import SwiftUI

struct CardDetailsView: View {
    
    var park: Park
    
    var body: some View {
        HStack {
            VStack {
                VStack {
                    Text("\(park.parkConditions[0].overallTotalRiders)")
                    Text("Total Riders Today").font(.caption)
                        .foregroundColor(.secondary)
                }
                .padding(.bottom)
                VStack {
                    Text("\(park.parkConditions[0].totalRiders)")
                    Text("Total Riders Past Hour").font(.caption)
                        .foregroundColor(.secondary)
                }
                .padding(.bottom)
                VStack {
                    Text(String(format:"%.2f",  park.parkConditions[0].rainVolumeInThree) + " mm")
                    Text("Total Rain Past 3 Hours").font(.caption)
                        .foregroundColor(.secondary)
                }
                .padding(.bottom)
                VStack {
                    Text("\(park.parkConditions[0].weatherConditions)")
                    Text("Current Weather").font(.caption)
                        .foregroundColor(.secondary)
                }
                .padding(.bottom)
                VStack {
                    Text("\(MathHelper.secondsToHoursMinutesSeconds(seconds: park.parkConditions[0].averageMovingTime))")
                    Text("Average Moving Time").font(.caption)
                        .foregroundColor(.secondary)
                }
                .padding(.bottom)
                Spacer()
            }
            .padding([.bottom, .top, .horizontal])
            VStack {
                VStack {
                    Text(String(format:"%.3f",  park.parkConditions[0].totalRainVolume) + " mm")
                    Text("Total Rain Past 48 hours").font(.caption)
                        .foregroundColor(.secondary)
                }
                .padding(.bottom)
                VStack {
                    Text(String(format:"%.3f",  park.parkConditions[0].rainVolumneInHour) + " mm")
                    Text("Total Rain Past Hour").font(.caption)
                        .foregroundColor(.secondary)
                }
                .padding(.bottom)
                VStack {
                    Text(String(format:"%.2f", MathHelper.convertKelvinToFahrenheit(kelvin: park.parkConditions[0].weatherTemp)))
                    Text("Temperature(F)").font(.caption)
                        .foregroundColor(.secondary)
                }
                .padding(.bottom)
                VStack {
                    Text(String(format:"%.2f",  park.parkConditions[0].humidity))
                    Text("Humidity").font(.caption)
                        .foregroundColor(.secondary)
                }
                .padding(.bottom)
                VStack {
                    Text("\(MathHelper.secondsToHoursMinutesSeconds(seconds: park.parkConditions[0].averageElapsedTime))")
                    Text("Average Elasped Time").font(.caption)
                        .foregroundColor(.secondary)
                }
                .padding(.bottom)
                Spacer()
            }
            .padding([.bottom, .top, .horizontal])
        }
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1)
        )
            .padding([.top, .bottom])
            .navigationBarTitle(Text(self.park.name), displayMode: .inline)
            .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct CardDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        CardDetailsView(park: Park(name: "Park", parkConditions: [
            ParkCondition()
        ]))
    }
}
