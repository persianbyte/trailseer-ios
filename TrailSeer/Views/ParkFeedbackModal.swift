//
//  ParkFeedbackModal.swift
//  TrailSeer
//
//  Created by SoapMac on 1/7/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import SwiftUI

struct ParkFeedbackModal: View {
    // 1. Add the environment variable
    @Environment(\.presentationMode) var presentationMode
    
    var strengths = ["Dry", "Tacky", "Wet"]
    
    @State private var selectedStrength = 0
    @State private var name = ""
    
    var body: some View {
        VStack {
            
            NavigationView {
                Form {
                    Section {
                        Picker(selection: $selectedStrength, label: Text("User Defined Conditions")) {
                            ForEach(0 ..< strengths.count) {
                                Text(self.strengths[$0]).tag(self.strengths[$0])
                            }
                        }
                    }
                    Section {
                        TextField(" Trail comments ex. muddy, unridable etc", text: $name)
                    }
                }
                .navigationBarTitle("Submit your feedback")
                .navigationBarItems(
                    leading: Button(action: {
                        print("dismissed form")
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        Text("Dismiss").bold()
                    },
                    trailing:
                    Button(action: {
                          print("submitted form")
                          self.presentationMode.wrappedValue.dismiss()
                      }) {
                          Text("Submit").bold()
                      })
            }.navigationViewStyle(StackNavigationViewStyle())
            
        }
    }
}

struct ParkFeedbackModal_Previews: PreviewProvider {
    static var previews: some View {
        ParkFeedbackModal()
    }
}
