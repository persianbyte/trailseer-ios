//
//  PrivacyPolicyView.swift
//  TrailSeer
//
//  Created by SoapMac on 1/16/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import SwiftUI

struct PrivacyPolicyView: View {
    var body: some View {
        WebView(request: URLRequest(url: URL(string: "https://www.trailseer.com/p/privacy-policy-body-font-family.html")!))          .navigationBarTitle(Text("Privacy Policy"), displayMode: .inline)
    }
}

struct PrivacyPolicyView_Previews: PreviewProvider {
    static var previews: some View {
        PrivacyPolicyView()
    }
}
