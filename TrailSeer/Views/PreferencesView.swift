//
//  PreferencesView.swift
//  TrailSeer
//
//  Created by SoapMac on 1/15/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import SwiftUI

struct PreferencesView: View {
    
    @State private var toggleAnalytics: Bool = true
    
    var version: String! = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
    
    var apiVersion: String = "1.0"
    
    var body: some View {
        Form {
            Section {
                VStack(alignment: .leading) {
                    Toggle(isOn: $toggleAnalytics) {
                        Text("Send Usage Data")
                    }
                    Text("Help improve the app").font(.caption)
                }
                
            }
            Section {
                NavigationLink(destination: Text("hello")) {
                    Text("Send Feedback")
                }
            }
            Section {
                NavigationLink(destination: Text("hello")) {
                    Text("About")
                }
            }
            Section {
                NavigationLink(destination: PrivacyPolicyView()) {
                    Text("Privacy Policy")
                }
            }
            Section {
                Text("TrailSeer Version " + self.version + " API " + self.apiVersion)
            }
        }
        .navigationBarTitle("Preferences")
    }
}

struct PreferencesView_Previews: PreviewProvider {
    static var previews: some View {
        PreferencesView()
    }
}
