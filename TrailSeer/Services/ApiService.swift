//
//  ApiService.swift
//  TrailSeer
//
//  Created by SoapMac on 1/27/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import Foundation
import SwiftUI


class ApiService {
    
    func post<T: Codable>(completion: @escaping (T) -> (), url: String, body: Data?) {
        self.request(completion: completion, urlStr: url, method: "POST", body: body)

    }
    
    func get<T: Codable>(completion: @escaping (T) -> (), url: String) {
        self.request(completion: completion, urlStr: url, method: "GET", body: nil)
    }
    
    private func request<T: Codable>(completion: @escaping((T) -> ()), urlStr: String, method: String, body: Data?) {
        let basePath = Bundle.main.object(forInfoDictionaryKey: "API_HOST_URL") as! String
        let newUrl = basePath + "/api/" + urlStr
        let url = URL(string: newUrl)
        guard let requestUrl = url else { fatalError() }
        // Prepare URL Request Object
        var request = URLRequest(url: requestUrl)
        request.httpMethod = method
        
        if(body != nil) {
            request.httpBody = body
        }
        
        // Set HTTP Request Header
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(UIDevice.current.identifierForVendor!.uuidString, forHTTPHeaderField: "instance_id")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
         
             if let data = data {
                 if let result = try? JSONDecoder().decode(T.self, from: data) {
                     // we have good data – go back to the main thread
                     DispatchQueue.main.async {
                         // update our UI
                         completion(result)
                     }
                 }
             }
        }.resume()
    }
}
