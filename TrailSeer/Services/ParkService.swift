//
//  ParkService.swift
//  TrailSeer
//
//  Created by SoapMac on 1/27/20.
//  Copyright © 2020 SoapCom. All rights reserved.
//

import Foundation


class ParkService: ApiService {
    
    func getParks(completion: @escaping ([Park]) -> ()) {
        self.get(completion: completion, url: "parks")
    }
}
